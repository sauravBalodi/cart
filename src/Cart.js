import React, { useState } from "react";
import "./Cart.css";
import Details from "./Details";
function Cart() {
  const [first, SetFirst] = useState(0);
  const [Sum, setSum] = useState(156);
  const [items, setItems] = useState(4);
  const [detail, setDetail] = useState(Details);
  const trash = (id) => {

    setSum(Sum - detail[id].price * detail[id].qty);

    setDetail(detail.filter((ele, idx) => idx !== id));
    setItems(items - 1);
  };



  return (
    <div className="cart">
      <div className="cart_back">
        <h2 >Your Cart</h2>
      </div>
      <div className="cart_quantities">
        <table className="table">
          <thead className="thead">
            <tr>
              <th>Product Name & Price</th>
              <th>Quantity</th>
              <th>Total</th>
              <th></th>
            </tr>
          </thead>
          <tbody >
            {detail.map((Detail, id) => {
              return (
                <>

                  <tr key={id}>
                    <td>
                      {Detail.name}
                      <p>${Detail.price}/mo</p>
                    </td>
                    <td>

                      <button
                        type="button"
                        onClick={() => (
                          SetFirst(Detail.price + Detail.price),
                          setSum(Sum + Detail.price),
                          (Detail.qty = Detail.qty + 1)
                        )}
                        className="quantity-right-plus btn  btn-number"
                        data-testid="add"
                      >
                        <span class="glyphicon glyphicon-plus"></span>
                      </button>
                      &#160;&#160;&#160;&#160;
                      <input disabled className="input" value={Detail.qty < 0 ? 0 : Detail.qty}/>
                      &#160; &#160;&#160;&#160;

                      {Detail.qty == 0 ? null : (
                        <button
                          type="button"
                          onClick={() => (
                            SetFirst(first - Detail.price),
                            setSum(Sum - Detail.price),
                            (Detail.qty = Detail.qty - 1)
                          )}
                          className="quantity-left-minus btn  btn-number"
                          data-testid="minus"
                        >
                          <span className="glyphicon glyphicon-minus"></span>
                        </button>
                      )}
                    </td>
                    <td>
                      <b>
                        {Detail.price * Detail.qty < 0
                          ? 0
                          : Detail.price * Detail.qty}
                        /mo
                      </b>
                    </td>
                    <td>
                      <button onClick={() => trash(id)}>
                        <i className="glyphicon glyphicon-trash"></i>
                      </button>
                    </td>
                  </tr>
                </>
              );
            })}
            <tr>
              <th>
                Numbe of Items:{items}
              </th>
              <th >
                
              </th>
              <pre>
              <th>Total: ${Sum}/mo</th>
              </pre>
              <th></th>
             
            </tr>
            

          </tbody>
        </table>
        <button class="button" onClick={() => console.log(Sum)}>Save</button>
      </div>
    </div>
  );
}
export default Cart;