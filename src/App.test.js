import { fireEvent, getByTestId, getByText, getQueriesForElement, render, screen } from '@testing-library/react';
import App from './App';
import ReactDom from 'react-dom';
import Cart from "./Cart.js";
import Details from "./Details";
test('renders', () => {

  // const linkElement = screen.getByText(/learn react/i);

  const root = document.createElement("div");
  ReactDom.render(<App />, root);

  const { getByText, getByLabelText } = getQueriesForElement(root);
  expect(getByText("Your Cart")).not.toBeNull();
});



describe("big test", () => {
  it("it is to check that cart component is rendered successfully", () => { <Cart /> });

  it("check first column is rendered or not", () => {
    const root = document.createElement("div");
    ReactDom.render(<Cart />, root);
    expect(root.querySelector("th").textContent).toBe("Product Name & Price");

    expect(root.querySelectorAll("th")[1].textContent).toBe("Quantity");
    expect(root.querySelectorAll("th")[2].textContent).toBe("Total");

  })
  it("json data from details file", () => {

    Details.map((cvalue, i) => {
      expect(cvalue.name).toBe(cvalue.name);
      expect(cvalue.price).toBe(cvalue.price);
      expect(cvalue.qty).toBe(cvalue.qty);
    })

  });

  it("save button ", () => {
    const root = document.createElement("div");
    const { getByText } = render(<Cart />);
    const button = root.querySelectorAll("button")[16];
    fireEvent.click(getByText('Save'), button);
  })
  it("plus buttons ", () => {
    const root = document.createElement("div");
    const { getAllByTestId } = render(<Cart />);
    ReactDom.render(<Cart />, root);
    const button = root.querySelectorAll("button")[0];


    fireEvent.click(screen.getAllByTestId("add")[0]);
  })

  it("subtraction buttons ", () => {
    const root = document.createElement("div");
    const { getAllByTestId } = render(<Cart />);
    ReactDom.render(<Cart />, root);
    const button = root.querySelectorAll("button")[0];
    fireEvent.click(screen.getAllByTestId("minus")[0]);
  })



})

